# Simple Makefile, based on built-in rules.

CC := clang
CFLAGS += -Wall -Wextra -Wpedantic
LDFLAGS += -lreadline

all: pearl

debug: CFLAGS += -DDEBUG -g
debug: pearl

run: pearl
	./$<

clean:
	rm pearl

.PHONY: debug run clean
