# pearl

What do you get when you open a shell? That's right, a pearl.

## Builtins
* `cd`
* `exit`

## TODO
* [ ] skip all trailing whitespaces, e.g. `ls____` where `_` is a whitespace. this currently fails.
* [x] CTRL-C should be picked up by the signal handler and relayed to the child process (e.g. ping)
* [x] use readline with history instead of simple buffer
* [x] correct handling of cd, pwd, etc.
