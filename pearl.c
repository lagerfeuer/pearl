#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include <readline/history.h>
#include <readline/readline.h>

#define MAX_ARG_COUNT 11
#define BUF_SIZE 256

int running = 1;
int exit_code = 0;

volatile pid_t curr_pid;

char cwd[BUF_SIZE];
char username[BUF_SIZE];
/* char command[256]; */
char args[MAX_ARG_COUNT][BUF_SIZE];
char buffer[BUF_SIZE] __attribute__((aligned(4096)));
char* split_args[MAX_ARG_COUNT][MAX_ARG_COUNT] = {{NULL}};
int pipeNumber = 0;

// ============================================================
// Signals
// ============================================================
void relay_handler(int sig) { kill(curr_pid, sig); }

// ============================================================
// ============================================================
void free_args() {
  for (int i = 0; i < MAX_ARG_COUNT; i++) {
    for (int j = 0; j < MAX_ARG_COUNT; j++) {
      free(split_args[i][j]);
      split_args[i][j] = NULL;
    }
  }
}

void handle_redirection(char*** cmd_args) {
  int index = 0;
  // iterate arguments looking for <, > and >>
  while ((*cmd_args)[index] != NULL) {
    int input_redir = strcmp((*cmd_args)[index], "<");
    int output_redir = strcmp((*cmd_args)[index], ">");
    int output_redir_append = strcmp((*cmd_args)[index], ">>");
    if (input_redir == 0) {
      int in_fd = open((*cmd_args)[index + 1], O_RDONLY);
      if (dup2(in_fd, STDIN_FILENO) == -1) {
        perror("pearl: error on stdin redirection");
        return;
      }
      if (close(in_fd) < 0) {
        perror("pearl: error on stdin close");
        return;
      }
    }
    if ((output_redir == 0) || (output_redir_append == 0)) {
      int file_flags = O_WRONLY | O_CREAT;
      if (output_redir_append == 0)
        file_flags |= O_APPEND;
      if (output_redir == 0)
        file_flags |= O_TRUNC;
      fflush(stdout);
      int out_fd = open((*cmd_args)[index + 1], file_flags, 0777);
      if (dup2(out_fd, STDOUT_FILENO) == -1) {
        perror("pearl: error on stdout redirection");
        return;
      }
      if (close(out_fd) < 0) {
        perror("pearl: error on stdout close");
        return;
      }
    }
    // free (<|>|>>) <file_name>
    if ((output_redir == 0) || (output_redir_append == 0) ||
        (input_redir == 0)) {
      free((*cmd_args)[index]);
      (*cmd_args)[index] = NULL;
      free((*cmd_args)[index + 1]);
      (*cmd_args)[index + 1] = NULL;
      index++;
    }
    index++;
  }
}

// ============================================================
// spawn_process
// ============================================================
int spawn_process(int in, int out, char* cmd_args[]) {
  pid_t pid;
  if ((pid = fork()) == 0) {
    // ===============================================
    // == CHILD PROCESS ==
    // ===============================================
    if (in != STDIN_FILENO) {
      if (dup2(in, STDIN_FILENO) < 0) {
        perror("pearl: stdin file descriptor error");
        return EXIT_FAILURE;
      }
      if (close(in) < 0) {
        perror("pearl: error on stdin close");
        return EXIT_FAILURE;
      }
    }
    if (out != STDOUT_FILENO) {
      if (dup2(out, STDOUT_FILENO) < 0) {
        perror("pearl: stdout file descriptor error");
        return EXIT_FAILURE;
      }
      if (close(out) < 0) {
        perror("pearl: error on stdout close");
        return EXIT_FAILURE;
      }
    }
    handle_redirection(&cmd_args);
    // ===============================================
    // == EXEC CALL ==
    // ===============================================
    execvp(cmd_args[0], cmd_args);
    perror("pearl: error on exec");
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    perror("Fork Error");
    exit(EXIT_FAILURE);
  } else {
    curr_pid = pid;
    waitpid(pid, NULL, 0);
    if (in != STDIN_FILENO) {
      if (close(in) < 0)
        perror("pearl: parent close in");
    }
    if (out != STDOUT_FILENO) {
      if (close(out) < 0)
        perror("pearl: parent close out");
    }
  }
  return 0;
}

void handle_command(char* buffer, int buffer_size) {
  int c = 0;
  int argsCount = 0;
  int lastIndex = 0;
  // determine if current position (space) is surrounded by quotes
  // quotes cannot be part of the argument vector
  int insideQuotes = 0, lastArgumentQuotes = 0;
  int offset = 0;
  // if not inside argument, skip spaces
  int insideArgument = 0;

  for (c = 0; c < buffer_size; c++) {
    if (argsCount > MAX_ARG_COUNT) {
      argsCount = MAX_ARG_COUNT;
      printf("Argument Count is limited to 10 (no dynamic memory allocation) "
             "all other arguments will be ignored\n");
      break;
    }
    if (buffer[c] == '"') {
      insideQuotes ^= 1;
      if (!insideQuotes)
        lastArgumentQuotes = 1;
    }
    if (!isspace(buffer[c]) && !insideArgument) {
      insideArgument = 1;
      lastIndex = c;
    }

    if ((isspace(buffer[c]) || buffer[c] == '\0') && !insideQuotes &&
        insideArgument) {
      if (lastArgumentQuotes) {
        lastArgumentQuotes = 0;
        offset = 1;
      }
      memcpy(args[argsCount], buffer + lastIndex + offset,
             c - lastIndex - 2 * offset);
      args[argsCount][c - lastIndex] = 0;
      argsCount++;
      lastIndex = c + 1;
      offset = 0;
      insideArgument = 0;
    }
  }
  // ===============================================
  // SPLIT PIPES
  // ===============================================
  int split_cnt = 0, split_args_cnt = 0;
  for (int i = 0; i < argsCount; i++) {
    if (strcmp(args[i], "|") == 0) {
      pipeNumber++;
      split_cnt++;
      split_args_cnt = 0;
      continue;
    }
    split_args[split_cnt][split_args_cnt] =
        (char*)malloc((sizeof(char) * strlen(args[i])) + 1);
    strcpy(split_args[split_cnt][split_args_cnt], args[i]);
    split_args[split_cnt][split_args_cnt][strlen(args[i])] = 0;
    split_args_cnt++;
  }
#ifdef DEBUG
  for (int i = 0; i < MAX_ARG_COUNT; i++) {
    for (int j = 0; j < MAX_ARG_COUNT; j++) {
      printf("%s\t", split_args[i][j]);
    }
    printf("\n");
  }
#endif
  // ===============================================
  // HANDLE COMMANDS / BUILT-INS / CALLS
  // ===============================================
  int in = STDIN_FILENO, out = STDOUT_FILENO;
  if (strcmp(args[0], "exit") == 0) {
    c = 4;
    while (buffer[c] == ' ')
      c++;
    exit_code = atoi(&buffer[c]);
    running = 0;
  }
  // shell builtins
  else if (strcmp(args[0], "cd") == 0) {
#ifdef DEBUG
    printf("\n'cd' to %s\n", args[1]);
#endif
    chdir(args[1]);
  }
  // ===============================================
  // FORK & EXEC
  // ===============================================
  else if (strcmp(args[0], "") != 0) {
    int pipes[pipeNumber * 2];
    int pipeCount = 0;
    for (int i = 0; i <= pipeNumber; i++) {
      if (pipeNumber > 0) {
        if (pipe(pipes + pipeCount) < 0) {
          perror("pearl: pipe error");
          return;
        }
        out = pipes[pipeCount + 1];
      }
      if (i == pipeNumber)
        out = STDOUT_FILENO;
      // SPAWN PROCESS
      spawn_process(in, out, split_args[i]);
      // ===============================================
      // == PARENT PROCESS ==
      // ===============================================
      if (pipeNumber > 0)
        in = pipes[pipeCount];
      pipeCount += 2;
    }
    /* int returnVal = WEXITSTATUS(status); */
  }
}

int main(int argc, char* argv[]) {
  signal(SIGINT, relay_handler);

  strcpy(username, getenv("USER"));
  char prompt[BUF_SIZE];

  do {
    char* input;
    getcwd(cwd, BUF_SIZE);
    snprintf(prompt, BUF_SIZE - 1, "\n%s - %s%s", username, cwd, "> ");

    input = readline(prompt);
    if (!input)
      break;
    add_history(input);
    handle_command(input, strlen(input) + 1);

    // free input
    free(input);
    // reset args array
    for (int i = 0; i < MAX_ARG_COUNT; i++)
      args[i][0] = '\0';
    free_args();
    pipeNumber = 0;
  } while (running);

  return exit_code;
}
